# language: es
Característica: Actualizar y obtener datos
  yo como cliente registrado
  deseo Acrtualizar datos
  para validar el funcionamiento de los servicios

  Escenario: Actualizar datos
    Dado el usuario esta en la plataforma y desea actualizar datos
    Cuando el usuario logra actualizar los datos
    Entonces el usuario obtendra un codigo de respuesta exitosa y podra ver sus datos actualizados

  Escenario: Obtener  datos
    Dado el usuario esta en la plataforma y desea obtener datos
    Cuando el usuario logra obtener los datos
    Entonces el usuario obtendra un codigo de respuesta exitosa y podra obtener los datos

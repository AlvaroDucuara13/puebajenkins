package co.com.sofka.model;

public class actualizarDatosRespuesta {
	private String name;
	private String job;
	private String updatedAt;

	public String getName(){
		return name;
	}

	public String getJob(){
		return job;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}
}

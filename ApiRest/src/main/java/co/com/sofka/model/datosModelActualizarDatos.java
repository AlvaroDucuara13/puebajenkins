package co.com.sofka.model;

public class datosModelActualizarDatos {

    private String FullName;
    private String Job;

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getJob() {
        return Job;
    }

    public void setJob(String job) {
        Job = job;
    }
}

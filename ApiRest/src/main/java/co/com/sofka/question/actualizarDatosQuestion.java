package co.com.sofka.question;


import co.com.sofka.model.actualizarDatosRespuesta;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class actualizarDatosQuestion implements Question {


    @Override
    public actualizarDatosRespuesta answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(actualizarDatosRespuesta.class);
    }
}

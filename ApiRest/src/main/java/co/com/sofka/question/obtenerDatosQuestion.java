package co.com.sofka.question;

import co.com.sofka.model.obtenerDatosPojo.userModel;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class obtenerDatosQuestion implements Question {



    @Override
    public userModel answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(userModel.class);
    }
}

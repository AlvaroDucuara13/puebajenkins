package co.com.sofka.task;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;

public class doObtenerDatos implements Task {
    private static String resource;

    public doObtenerDatos usingTheResources(String resource) {
        doObtenerDatos.resource = resource;
        return this;
    }



    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource(resource)
                        .with(
                                requestSpecification -> requestSpecification.contentType(ContentType.JSON)
                                        .log()
                                        .all()
                        )
        );

    }

    public static doObtenerDatos doGet(){
        return new doObtenerDatos();
    }
}
